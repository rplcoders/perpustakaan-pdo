<?php
	// mencegah akses ke halaman tertentu bagi yang belum login,
    // atau tidak memiliki hak akses yang tidak sesuai
	function batasiAkses($level=[1,2,3,4,5,6,7,8,9]) {
		if (!isset($_SESSION['username'])) { // jika belum login
        	header("Location: error.php"); exit;
    	} else { // sudah login, tapi perlu dicek levelnya
    		if (!in_array($_SESSION["level"], $level)) {
    			setPesan("Anda tidak memiliki level user yang cukup untuk masuk halaman ".$_SERVER["REQUEST_URI"]);
    			header("Location: admin.php"); exit;
    		}
    	}
	}

	// membatasi tampilan hanya untuk user dengan level tertentu
	function batasiTampilan($level=[1,2,3,4,5,6,7,8,9]) {
        if (!isset($_SESSION['username'])) { // jika belum login
            return false;
        } else { // sudah login, tapi perlu dicek levelnya
            if (!in_array($_SESSION["level"], $level)) {
                return false;
            } else {
                return true;
            }
        }
    }

    // menyimpan pesan untuk dijadikan flash message
	function setPesan($pesan) {
		$_SESSION['msg'] = $pesan; // menyimpan session utk pesan
	}

	// menampilkan pesan dari flash message
	function tampilPesan() {
		if (isset($_SESSION['msg'])) { // mengecek ada tidaknya pesan
			echo '<div class="alert alert-success">'.
				 $_SESSION['msg'].'</div>'; // menampilkan pesan dengan style alert
			unset($_SESSION['msg']); // menghapus pesan stlh ditampilkan
		}
	}

	// mendapatkan nama lengkap user / petugas
	function getNamaLengkap($username) {
		global $conn;
		$sql = "SELECT * FROM petugas WHERE username = ?";
        $stmt = $conn->prepare($sql);
        $stmt->execute([$username]);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();
        if (count($result) > 0) {
            return $result[0]["nama"];
        } else {
            return "User tidak ditemukan";
        }
	}

	// mengecek apakah pengakses sudah login atau belum
	function isLoggedIn() {
	    return (isset($_SESSION['username'])) ? true : false;
    }

    // debug variabel
	function cekvar($v) {
        echo "<pre>"; print_r($v); echo "</pre>"; exit;
    }
?>