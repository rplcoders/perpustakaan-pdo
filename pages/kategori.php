<!-- pages/kategori.php -->
<div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Kategori Buku</h1>           
          </div>
			
          <?php tampilPesan(); ?>
          <?php if(batasiTampilan()): // hanya menampilkan
              // untuk user yang sudah login dengan level 1 dan 9 ?>
          <!-- Content Row -->
          <div class="row">
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Entry Data Kategori Buku</h6>
                </div>
                <?php // mengambil baris data untuk diedit, jika terdeteksi parameter URL id
                	$data = false;
                    if (isset($_GET["id"])) {
                		$id = $_GET["id"];
                		$stmt = $conn->prepare("SELECT * FROM kategori_buku 
                                                WHERE Kode_Kategori = ?");
                		$stmt->execute([$id]);
                		$data = $stmt->fetch(PDO::FETCH_OBJ);
                	}
                ?>
                <!-- Card Body -->
                <div class="card-body">
                   <form action="proses/kategori-proses.php" method="post">
					<div class="form-group">
                        <label>Nama Kategori</label>
						<input type="text" name="nama_kategori" class="form-control"
						value="<?= ($data) ? $data->Nama_Kategori : ''; ?>">
						<input type="hidden" name="kode_kategori"
						value="<?= ($data) ? $data->Kode_Kategori : 0; ?>">
                    </div>
					<input type="submit" class="btn btn-success" value="Simpan">
				   </form>
                </div>
              </div>
           </div>          
		  </div> <!-- end row untuk form -->
	      <?php endif; ?>

		  <div class="row"> <!-- start row untuk tabel -->
			<div class="col-xl-12 col-md-12 mb-12">
				<table class="table table-striped" id="tabel_kategori">
					<thead>
					<tr>
						<th>Kode</th>
                        <th>Nama Kategori</th>
                        <th>Jumlah Buku</th>
                        <th>Aksi</th>
					</tr>
					</thead>
					<tbody>
					<?php // mengambil data untuk ditampilkan di tabel
						$sql = "SELECT *, 
                                    (SELECT COUNT(*) 
                                    FROM buku 
                                    WHERE Bk_Kode_Kategori = Kode_Kategori) AS Jml_Buku
                                FROM kategori_buku 
								ORDER BY Kode_Kategori DESC";
						$stmt = $conn->query($sql);
						while ($row = $stmt->fetch(PDO::FETCH_OBJ)):
					?>
					<tr>
						<td><?= $row->Kode_Kategori; ?></td>
						<td><?= $row->Nama_Kategori; ?></td>
                        <td><?= $row->Jml_Buku; ?></td>
						<td>
                            <?php if(batasiTampilan([1,9])): ?>
							<a href="?p=kategori&id=<?= $row->Kode_Kategori; ?>" class="btn btn-sm btn-warning">Edit</a>
							<a href="proses/kategori-proses.php?id=<?= $row->Kode_Kategori; ?>" class="btn btn-sm btn-danger confirm">Hapus</a>
						    <?php endif; ?>
                            <a href="pages/modal-kategori.php?id=<?= $row->Kode_Kategori; ?>" class="btn btn-sm btn-success toggle-modal">Detail</a>
                        </td>
					</tr>	
						<?php endwhile; ?>
					</tbody>
				</table>
			</div>
		  </div> <!-- end row untuk tabel -->
        <!-- /.container-fluid -->
</div>
<script>
    $(document).ready(function() {
        $('#tabel_kategori').DataTable({
            "language": {
                "url": "http://localhost/pwd2019/perpustakaan/vendor/datatables/Indonesian.json"
            }
        });
        $('a.toggle-modal').bind('click', function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            if (url.indexOf('#') == 0) {
                $('#modal_content').modal('open');
                // $('.editor').wysihtml5();
            } else {
                $.get(url, function (data) {
                    $('#modal_content').modal();
                    $('#modal_content').html(data);
                });
            }
        });
    });

$('a.confirm').bind('click',function(e) {
	var baris = $(this).parents('tr');
	e.preventDefault();
	var url = $(this).attr('href');
	Swal.fire({
	  title: 'Anda yakin untuk menghapus?',
	  text: "Data yang sudah dihapus tidak dapat dikembalikan lagi.",
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Ya, saya yakin',
      cancelButtonText: 'Tidak, saya khilaf'
	}).then((result) => {
	  if (result.value) {
		$.get(url).done(function(response) {
			Swal.fire('Data berhasil dihapus').then((result) => {
					baris.remove();
				});
		});
	  }
	});

});
</script>