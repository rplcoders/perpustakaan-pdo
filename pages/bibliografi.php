<!-- pages/kategori.php -->
<div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Bibliografi Buku</h1>           
          </div>
             <?php tampilPesan(); ?>
          <!-- Content Row -->
          <div class="row">
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Entry Data Bibliografi</h6>
                </div>
                  <?php // mengambil baris data untuk diedit, jika terdeteksi parameter URL id
                  $data = false;
                  if (isset($_GET["id"])) {
                      $id = $_GET["id"];
                      $stmt = $conn->prepare("SELECT * FROM buku 
                                                WHERE Kode_Buku = ?");
                      $stmt->execute([$id]);
                      $data = $stmt->fetch(PDO::FETCH_OBJ);
                  }
                  ?>
                <!-- Card Body -->
                <div class="card-body">
                   <form action="proses/biblio-proses.php" method="post" enctype="multipart/form-data">
					<div class="form-group">
                        <label>Judul Buku</label>
						<input type="text" name="judul_buku" class="form-control"
                        value="<?= ($data) ? $data->Bk_Judul_Buku : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label>Penulis</label>
						<input type="text" name="penulis" class="form-control"
                        value="<?= ($data) ? $data->Bk_Penulis : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label>Peberbit</label>
						<input type="text" name="penerbit" class="form-control"
                        value="<?= ($data) ? $data->Bk_Penerbit : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label>Tahun Terbit</label>
						<input type="text" name="th_terbit" class="form-control"
                        value="<?= ($data) ? $data->Bk_Th_Terbit : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label>Kategori</label>
                        
						<select name="kode_kategori" class="form-control" id="select_kategori">
                        <?php 
						$sql = "SELECT * FROM kategori_buku 
								ORDER BY Nama_Kategori ASC";
						$stmt = $conn->query($sql);
						while ($row = $stmt->fetch(PDO::FETCH_OBJ)):
					    ?>
                            <option value="<?= $row->Kode_Kategori; ?>"
                             <?= ($data && ($data->Bk_Kode_Kategori == $row->Kode_Kategori)) ?
                                 "selected" : ''; ?>>
                                <?= $row->Nama_Kategori; ?></option>
                        <?php endwhile; ?>
                        </select>
                    </div>
                   <div class="form-group">
                       <label>Cover (jika ada)</label>
                       <input type="file" name="file_cover" class="form-control">
                   </div>
                    <input type="hidden" name="kode_buku"
                           value="<?= ($data) ? $data->Kode_Buku : 0; ?>">
                    <input type="hidden" name="cover_existing"
                           value="<?= ($data) ? $data->Bk_Foto : ''; ?>">
					<input type="submit" class="btn btn-success" value="Simpan">
				   </form>
                </div>
              </div>
           </div>          
		  </div> <!-- end row untuk form -->
		  
		  <div class="row"> <!-- start row untuk tabel -->
			<div class="col-xl-12 col-md-12 mb-12">
				<table class="table table-striped" id="tabel_biblio">
					<thead>
					<tr>
						<th>Kode</th>
                        <th>Judul Buku</th>
                        <th>Tahun Terbit</th>
                        <th>Kategori</th>
                        <th>Aksi</th>
					</tr>
					</thead>
					<tbody>
					<?php 
						$sql = "SELECT * FROM buku
                                LEFT JOIN kategori_buku 
                                ON Kode_Kategori = Bk_Kode_Kategori 
								ORDER BY Kode_Buku DESC";
						$stmt = $conn->query($sql);
						while ($row = $stmt->fetch(PDO::FETCH_OBJ)):
					?>
					<tr>
						<td><?= $row->Kode_Buku; ?></td>
						<td><?= $row->Bk_Judul_Buku; ?>
                            <?php if ($row->Bk_Foto != "") { ?>
                                <br>
                                <img src="img/coverbuku/<?= $row->Bk_Foto ?>" width="80">
                            <?php  } ?>
                        </td>
                        <td><?= $row->Bk_Th_Terbit; ?></td>
                        <td><?= $row->Nama_Kategori; ?></td>
                        <td>
                            <a href="proses/biblio-detail.php?id=<?= $row->Kode_Buku; ?>" data-remote="false" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">Detail</a>
                            <a href="?p=bibliografi&id=<?= $row->Kode_Buku; ?>" class="btn btn-sm btn-warning">Edit</a>
                            <a href="proses/biblio-proses.php?id=<?= $row->Kode_Buku; ?>"
                               class="btn btn-sm btn-danger"
                               onclick="return confirm('Apakah Anda yakin untuk menghapus ?')">Hapus</a>
                        </td>
					</tr>	
						<?php endwhile; ?>
					</tbody>
				</table>
			</div>
		  </div> <!-- end row untuk tabel -->
        <!-- /.container-fluid -->
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Buku</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<script>
    $(document).ready(function() {
       $('#select_kategori').select2();
       $('#tabel_biblio').DataTable({
           "language": {
               "url": "http://localhost/pwd2019/perpustakaan/vendor/datatables/Indonesian.json"
           }
       });
        $("#myModal").on("show.bs.modal", function(e) {
            var link = $(e.relatedTarget);
            $(this).find(".modal-body").load(link.attr("href"));
        });
    });
</script>