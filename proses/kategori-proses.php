<?php 
	// file proses/kategori-proses.php
	require_once("../config.php");
	if (isset($_POST["nama_kategori"])) { // mendeteksi pengiriman form
		extract($_POST); // ekstraksi array $_POST
		if ($kode_kategori > 0) { // apakah mau mengupdate
			$sql = "UPDATE kategori_buku SET Nama_Kategori =  ? 
			        WHERE Kode_Kategori = ?";
	        $stmt = $conn->prepare($sql);
	        $stmt->execute([$nama_kategori, $kode_kategori]);
			setPesan("Data berhasil diperbarui");
		} else { // menambahkan data baru
			$sql = "INSERT INTO kategori_buku VALUES(NULL, ?)";
	        $stmt = $conn->prepare($sql);
	        $stmt->execute([$nama_kategori]) or die(print_r($stmt->errorInfo(), true));
			setPesan("Data berhasil disimpan");
		}	
		header("Location: ../index.php?p=kategori"); exit;
	}
    if (isset($_GET["id"])) { // mendeteksi operasi hapus
        $id = $_GET["id"];
        $stmt = $conn->prepare("DELETE FROM kategori_buku 
                                WHERE Kode_Kategori = ?");
        $stmt->execute([$id]);
    }
?>