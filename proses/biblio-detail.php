<?php
require_once("../config.php");
if (isset($_GET["id"])) { // mendeteksi permintaan detail
    $id = $_GET["id"];
    $stmt = $conn->prepare("SELECT * FROM buku 
                                                WHERE Kode_Buku = ?");
    $stmt->execute([$id]);
    $data = $stmt->fetch(PDO::FETCH_OBJ);
    // print_r($data);
}
?>
<table class="table table-striped">
    <tr>
        <td colspan="2" class="text-center">
            <?php if ($data->Bk_Foto != "") { ?>
                <img src="img/coverbuku/<?= $data->Bk_Foto ?>" width="240">
            <?php  } ?>
        </td>
    </tr>
    <tr>
        <th>Judul</th><td><?= $data->Bk_Judul_Buku; ?></td>
    </tr>
    <tr>
        <th>Tahun Terbit</th><td><?= $data->Bk_Th_Terbit; ?></td>
    </tr>
    <tr>
        <th>Penerbit</th><td><?= $data->Bk_Penerbit; ?></td>
    </tr>
    <tr>
        <th>Penulis</th><td><?= $data->Bk_Penulis; ?></td>
    </tr>
</table>
