<?php
// file proses/biblio-proses.php
require_once("../config.php");

// batasiAkses([1,9]);
if (isset($_POST["judul_buku"])) { // mendeteksi pengiriman form
    extract($_POST); // ekstraksi array $_POST
    // mengecek apakah ada upload file
    if ($_FILES['file_cover']['name'] != "") { // terdeteksi ada upload
        $upload_dir = "../img/coverbuku/";
        $namafilecover = mktime().$_FILES['file_cover']['name'];
        move_uploaded_file(
            $_FILES['file_cover']['tmp_name'],
            $upload_dir.$namafilecover
        );
    } else { // tidak ada file diupload
        if ($kode_buku > 0) { // uperasi update, jangan ubah nama file
            $namafilecover = $cover_existing;
        } else { // operasi insert, isi nama file dengan null
            $namafilecover = NULL;
        }
    }
    if ($kode_buku > 0) { // apakah mau mengupdate
        $sql = "UPDATE buku 
                SET Bk_Judul_Buku =  ?,
                Bk_Penulis = ?,
                Bk_Th_Terbit = ?,
                Bk_Kode_Kategori = ?,
                Bk_Penerbit = ?,
                Bk_Foto = ? 
			    WHERE Kode_Buku = ?";
        $stmt = $conn->prepare($sql);
        $formdata = [
            $judul_buku,
            $penulis,
            $th_terbit,
            $kode_kategori,
            $penerbit,
            $namafilecover,
            $kode_buku
        ];
        $stmt->execute($formdata);
        setPesan("Data berhasil diperbarui");
    } else { // menambahkan data baru
        $sql = "INSERT INTO buku VALUES(NULL, ?, ?, ?, ?, ?, ?)";
        $stmt = $conn->prepare($sql);
        $formdata = [
            $judul_buku,
            $penulis,
            $th_terbit,
            $kode_kategori,
            $penerbit,
            $namafilecover,
        ];
        $stmt->execute($formdata);
        setPesan("Data berhasil disimpan");
    }
    header("Location: ../index.php?p=bibliografi"); exit;
}
if (isset($_GET["id"])) { // mendeteksi operasi hapus
    $id = $_GET["id"];
    $stmt = $conn->prepare("DELETE FROM buku 
                                WHERE Kode_Buku = ?");
    $stmt->execute([$id]);
    setPesan("Data berhasil dihapus");
    header("Location: ../index.php?p=bibliografi"); exit;
}
