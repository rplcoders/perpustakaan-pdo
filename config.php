<?php
    if (!isset($_SESSION)) { session_start(); }
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "rpl_perpustakaan";

	try {
    	$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    	// echo "Connected successfully";
    }
		catch(PDOException $e)
    {
    	echo "Connection failed: " . $e->getMessage();
    }

    require_once("functions.php");
?>
